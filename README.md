Source : https://github.com/stevekane/promise-it-wont-hurt

Setup with `npm install`, run with `npx promise-it-wont-hurt`

Once complete, to go further: read https://developers.google.com/web/fundamentals/primers/promises#parallelism_and_sequencing_getting_the_best_of_both
(section **Parallelism and sequencing**)
