function onReject(error) {
    console.log(error.message);
}

function onFulfilled(response) {
    console.log(response);
}
function attachTitle(name) {
    return "DR. " + name;
}
const promise = Promise.resolve("MANHATTAN");

promise.then(attachTitle).then(onFulfilled);
