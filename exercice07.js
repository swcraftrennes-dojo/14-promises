function onReject(error) {
    console.log(error.message);
}

function onFulfilled(response) {
    console.log(response);
}

first()
    .then(second)
    .then(onFulfilled)
    .catch(onReject);
