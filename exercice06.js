const promise = Promise.reject(new Error("REJET !"));

function onReject(error) {
    console.log(error.message);
}

promise.then(console.log).catch(onReject);
