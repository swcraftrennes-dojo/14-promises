function parsePromised(jsonString) {
    return new Promise(function(fullfill, reject) {
        try {
            let parsedJson = JSON.parse(jsonString);
            fullfill(parsedJson);
        } catch (error) {
            reject(error);
        }
    });
}

function onReject(error) {
    console.log(error.message);
}

parsePromised(process.argv[2]).catch(onReject);
